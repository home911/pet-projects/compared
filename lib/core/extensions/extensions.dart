/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

extension DoubleParseWithComma on String {
  double? tryParseToDouble() {
    return double.tryParse(replaceAll(',', '.'));
  }

  double parseToDouble() {
    return double.parse(replaceAll(',', '.'));
  }
}