/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:logger/logger.dart';

class Log extends Logger {
  Log() : super(printer: PrettyPrinter());
}
