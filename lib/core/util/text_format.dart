/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:intl/intl.dart';

String overflowText(String text, [int maxLength = 3, String placeholder = '+999']) {
  if (text.length > maxLength) return placeholder;
  return text;
}

NumberFormat priceFormat() => NumberFormat.currency(locale: 'ru', decimalDigits: 2, symbol: '₽');
