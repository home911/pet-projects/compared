/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

/// Утилита для показа уведомлений
abstract class ToastUtil {
  /// Уведомление об ошибке
  void e(dynamic message);

  /// Уведомление с информацией
  void i(dynamic message);

  /// Уведомление об успехе
  void s(dynamic message);
}