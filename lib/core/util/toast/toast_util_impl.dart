/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:compared/core/core.dart';
import 'package:flutter/material.dart';

final messageKey = GlobalKey<ScaffoldMessengerState>();

class ToastUtilImpl extends ToastUtil {
  @override
  void e(message) {
    messageKey.currentState?.clearSnackBars();
    messageKey.currentState?.showSnackBar(_snackBar(message, Theme.of(messageKey.currentContext!).colorScheme.error));
  }

  @override
  void i(message) {
    messageKey.currentState?.clearSnackBars();
    messageKey.currentState?.showSnackBar(_snackBar(message));
  }

  @override
  void s(message) {
    messageKey.currentState?.clearSnackBars();
    messageKey.currentState?.showSnackBar(_snackBar(message, Theme.of(messageKey.currentContext!).custom.success));
  }

  SnackBar _snackBar(dynamic message, [Color? backgroundColor]) {
    return SnackBar(
      duration: const Duration(seconds: 4),
      behavior: SnackBarBehavior.floating,
      margin: const EdgeInsets.all(MPadding),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(MRadius)),
      content: (message is Widget) ? message : Text(message),
      backgroundColor: backgroundColor,
    );
  }
}
