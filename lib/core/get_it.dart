/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:compared/core/core.dart';
import 'package:compared/features/main/data/service/product_service.dart';
import 'package:compared/features/main/data/service/product_service_impl_hive.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.I;

void setupGetIt() {
  getIt.registerLazySingleton<ProductService>(() => ProductServiceHive());
  getIt.registerLazySingleton<Log>(() => Log());
}