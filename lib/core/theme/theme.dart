/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:flex_color_scheme/flex_color_scheme.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:riverpod/riverpod.dart';

final themeModeProvider = StateProvider<ThemeMode>((ref) => ThemeMode.light);

final themeLightProvider = Provider<ThemeData>(
  (ref) => _themeLight.copyWith(
    iconTheme: IconThemeData(color: _themeLight.primaryColor),
    listTileTheme: ListTileThemeData(iconColor: _themeLight.primaryColor),
    inputDecorationTheme: _themeLight.inputDecorationTheme.copyWith(
      iconColor: _themeLight.primaryColor,
      prefixIconColor: _themeLight.primaryColor,
      suffixIconColor: _themeLight.primaryColor,
      isDense: true,
    ),
  ),
);

final themeDarkProvider = Provider<ThemeData>(
  (ref) => _themeDark..copyWith(
    iconTheme: IconThemeData(color: _themeDark.primaryColorLight),
    listTileTheme: ListTileThemeData(iconColor: _themeDark.primaryColorLight),
    inputDecorationTheme: _themeDark.inputDecorationTheme.copyWith(
      iconColor: _themeDark.primaryColorLight,
      prefixIconColor: _themeDark.primaryColorLight,
      suffixIconColor: _themeDark.primaryColorLight,
      isDense: true,
    ),
  ),
);

final _themeLight = FlexThemeData.light(
  colorScheme: ColorScheme.fromSeed(seedColor: const Color(0xFF3D40EF)),
  surfaceMode: FlexSurfaceMode.levelSurfacesLowScaffoldVariantDialog,
  blendLevel: 20,
  subThemesData: const FlexSubThemesData(
    navigationBarHeight: 60,
    blendOnLevel: 10,
    appBarCenterTitle: true,
    inputDecoratorIsFilled: false,
    inputDecoratorBorderType: FlexInputBorderType.underline,
    adaptiveAppBarScrollUnderOff: FlexAdaptive.all(),
    adaptiveRemoveElevationTint: FlexAdaptive.all(),
    blendOnColors: true,
  ),
  visualDensity: FlexColorScheme.comfortablePlatformDensity,
  useMaterial3: true,
  swapLegacyOnMaterial3: true,
  fontFamily: GoogleFonts.roboto().fontFamily,
);


final _themeDark = FlexThemeData.dark(
  colorScheme: ColorScheme.fromSeed(seedColor: const Color(0xFF2D2FDA), brightness: Brightness.dark),
  surfaceMode: FlexSurfaceMode.levelSurfacesLowScaffoldVariantDialog,
  blendLevel: 20,
  subThemesData: const FlexSubThemesData(
    navigationBarHeight: 60,
    blendOnLevel: 20,
    appBarCenterTitle: true,
    inputDecoratorIsFilled: false,
    inputDecoratorBorderType: FlexInputBorderType.underline,
    adaptiveAppBarScrollUnderOff: FlexAdaptive.all(),
    adaptiveRemoveElevationTint: FlexAdaptive.all(),
    blendOnColors: false,
  ),
  visualDensity: FlexColorScheme.comfortablePlatformDensity,
  useMaterial3: true,
  swapLegacyOnMaterial3: true,
  fontFamily: GoogleFonts.roboto().fontFamily,
);

extension CustomTheme on ThemeData {
  String get proteinsImg => brightness == Brightness.light ? 'assets/proteins.png' : 'assets/proteins_dark.png';
  String get fatsImg => brightness == Brightness.light ? 'assets/fats.png' : 'assets/fats_dark.png';
  String get carbohydratesImg => brightness == Brightness.light ? 'assets/carbohydrates.png' : 'assets/carbohydrates_dark.png';
  String get dishImg => brightness == Brightness.light ? 'assets/dish.png' : 'assets/dish_dark.png';
  String get productImg => 'assets/product.png';
  String get recipeImg => 'assets/recipe.png';
  CustomThemeData get custom => const CustomThemeData(
        favorite: Color(0xffe3022c),
        success: Color(0xff249411),
      );
}

class CustomThemeData {
  final Color favorite;
  final Color success;

  const CustomThemeData({
    required this.favorite,
    required this.success,
  });
}
