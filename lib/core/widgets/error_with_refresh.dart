/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ErrorWithRefresh extends ConsumerWidget {
  const ErrorWithRefresh({
    required this.errorMessage,
    required this.onRefresh,
    super.key,
  });

  final String errorMessage;
  final Function() onRefresh;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Center(
      child: Column(
        children: [Text(errorMessage), TextButton(onPressed: onRefresh, child: const Text('Обновить'))],
      ),
    );
  }
}
