/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

const SPadding = 4.0;
const MPadding = 8.0;
const LPadding = 12.0;
const XLPadding = 16.0;
const XXLPadding = 20.0;
const XXXLPadding = 24.0;

const SRadius = 4.0;
const MRadius = 8.0;
const LRadius = 12.0;

const SElevation = 4.0;
const MElevation = 8.0;
const LElevation = 12.0;