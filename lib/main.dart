/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:compared/features/main/presentation/page/main_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'core/core.dart';

final container = ProviderContainer();

void main() async {
  await Hive.initFlutter();
  setupGetIt();
  runApp(ProviderScope(
    parent: container,
    child: const MyApp(),
  ));
}

class MyApp extends ConsumerWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return MaterialApp(
      title: 'Сравнение цен',
      scaffoldMessengerKey: messageKey,
      theme: ref.read(themeLightProvider),
      darkTheme: ref.read(themeDarkProvider),
      themeMode: ref.watch(themeModeProvider),
      home: const MainPage(),
    );
  }
}
