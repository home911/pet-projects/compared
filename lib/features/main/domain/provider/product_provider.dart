/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:collection/collection.dart';
import 'package:compared/core/core.dart';
import 'package:compared/features/main/data/service/product_service.dart';
import 'package:compared/features/main/domain/model/create_product.dart';
import 'package:compared/features/main/domain/model/product.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:uuid/uuid.dart';
import 'product_state.dart';

part 'product_provider.g.dart';

@Riverpod(keepAlive: true)
class Product extends _$Product {
  final _service = getIt<ProductService>();

  @override
  FutureOr<ProductState> build() async {
    try {
      return ProductState(products: await _service.getAll());
    } catch (e, st) {
      getIt<Log>().e('Ошибка при инициализации продуктов', error: e, stackTrace: st);
      rethrow;
    }
  }

  Future<void> sort() async {
    try {
      if (state.isLoading) return;
      state = const AsyncLoading<ProductState>().copyWithPrevious(state);
      final prevState = await future;
      state = AsyncData(
        prevState.copyWith(
          products: [...prevState.products.sortedByCompare((element) => element.priceForOne, (a, b) => a.compareTo(b))],
        ),
      );
    } catch (e, st) {
      getIt<Log>().e('Ошибка при сортировке продуктов', error: e, stackTrace: st);
      rethrow;
    }
  }

  Future<void> clearProducts() async {
    try {
      if (state.isLoading) return;
      state = const AsyncLoading<ProductState>().copyWithPrevious(state);
      await _service.deleteAll();
      state = const AsyncData(ProductState(products: []));
    } catch (e, st) {
      getIt<Log>().e('Ошибка при очистке списка продуктов', error: e, stackTrace: st);
    }
  }

  Future<void> deleteProduct(String productUUID) async {
    try {
      if (state.isLoading) return;
      state = const AsyncLoading<ProductState>().copyWithPrevious(state);
      final prevState = await future;
      await _service.delete(productUUID);
      state = AsyncData(
        prevState.copyWith(products: [...prevState.products.where((element) => element.uuid != productUUID)]),
      );
    } catch (e, st) {
      getIt<Log>().e('Ошибка при удалении продукта', error: e, stackTrace: st);
      rethrow;
    }
  }

  Future<void> saveProduct(CreateProductModel product) async {
    try {
      if (state.isLoading) return;
      state = const AsyncLoading<ProductState>().copyWithPrevious(state);
      final prevState = await future;
      state = AsyncData(
        prevState.copyWith(
          products: [
            ...prevState.products,
            await _service.save(ProductModel(
              uuid: const Uuid().v4(),
              title: product.title,
              price: product.price,
              amount: product.amount,
            )),
          ],
        ),
      );
    } catch (e, st) {
      getIt<Log>().e('Ошибка при сохранении продукта', error: e, stackTrace: st);
      rethrow;
    }
  }

  Future<void> updateProduct(ProductModel product) async {
    try {
      if (state.isLoading) return;
      state = const AsyncLoading<ProductState>().copyWithPrevious(state);
      final prevState = await future;
      final updatedProduct = await _service.update(product.uuid, product);
      state = AsyncData(
        prevState.copyWith(products: [...prevState.products.map((e) => e.uuid == updatedProduct.uuid ? updatedProduct : e)]),
      );
    } catch (e, st) {
      getIt<Log>().e('Ошибка при обновлении продукта', error: e, stackTrace: st);
      rethrow;
    }
  }
}
