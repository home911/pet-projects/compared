// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$productHash() => r'841373d3c73299700ae11f21d1953d400650f6bb';

/// See also [Product].
@ProviderFor(Product)
final productPod = AsyncNotifierProvider<Product, ProductState>.internal(
  Product.new,
  name: r'productPod',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$productHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$Product = AsyncNotifier<ProductState>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
