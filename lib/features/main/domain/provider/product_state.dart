/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:equatable/equatable.dart';

import '../model/product.dart';

class ProductState extends Equatable {
  final List<ProductModel> products;

  const ProductState({
    required this.products,
  });

  ProductModel productByUUID(String uuid) => products.firstWhere((element) => element.uuid == uuid);

  List<({ProductModel product, double priceDifference, double discount})> benefitRelativeToOtherProducts(ProductModel product) {
    final otherProducts = products.where((element) => element.uuid != product.uuid);
    return otherProducts
        .map((e) => (
              product: e,
              priceDifference: e.priceForOne - product.priceForOne,
              discount: e.benefitRelativeToProduct(product),
            ))
        .toList();
  }

  ProductState copyWith({
    List<ProductModel>? products,
  }) {
    return ProductState(
      products: products ?? this.products,
    );
  }

  @override
  List<Object?> get props => [products];
}
