/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:compared/core/constants/hive_type_ids.dart';
import 'package:equatable/equatable.dart';
import 'package:hive_flutter/hive_flutter.dart';
part 'product.g.dart';

@HiveType(typeId: HIVE_ID_PRODUCT)
class ProductModel extends Equatable {
  @HiveField(0)
  final String uuid;
  @HiveField(1)
  final String title;
  @HiveField(2)
  final double price;
  @HiveField(3)
  final double amount;

  const ProductModel({
    required this.uuid,
    required this.title,
    required this.price,
    required this.amount,
  });

  /// Цена за единицу товара
  double get priceForOne => price == 0 || amount == 0 ? 0 : price / amount;

  /// Количество товара за единицу стоимости
  double get piecesPerPriceUnit => price == 0 || amount == 0 ? 0 : amount / price;

  /// Выгода относительно другого товара (значение от 0 до 1)
  double benefitRelativeToProduct(ProductModel product) {
    return 1 - (product.priceForOne / priceForOne);
  }

  ProductModel copyWith({
    String? uuid,
    String? title,
    double? price,
    double? amount,
  }) {
    return ProductModel(
      uuid: uuid ?? this.uuid,
      title: title ?? this.title,
      price: price ?? this.price,
      amount: amount ?? this.amount,
    );
  }

  @override
  List<Object?> get props => [uuid, title, price, amount];
}