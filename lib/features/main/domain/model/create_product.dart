/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:equatable/equatable.dart';

class CreateProductModel extends Equatable {
  final String title;
  final double price;
  final double amount;

  const CreateProductModel({
    required this.title,
    required this.price,
    required this.amount,
  });

  @override
  List<Object?> get props => [title, price, amount];
}