/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:compared/core/constants/hive_type_ids.dart';
import 'package:compared/core/constants/storage_keys.dart';
import 'package:compared/features/main/data/service/product_service.dart';
import 'package:compared/features/main/domain/model/product.dart';
import 'package:hive_flutter/hive_flutter.dart';

class ProductServiceHive extends ProductService {
  Future<Box<ProductModel>> _getBox() async {
    if (!Hive.isAdapterRegistered(HIVE_ID_PRODUCT)) {
      Hive.registerAdapter(ProductModelAdapter());
    }
    if (await Hive.boxExists(STORAGE_PRODUCTS) && Hive.isBoxOpen(STORAGE_PRODUCTS)) {
      return Hive.box<ProductModel>(STORAGE_PRODUCTS);
    }
    return await Hive.openBox(STORAGE_PRODUCTS);
  }

  @override
  Future<void> delete(String uuid) async => await (await _getBox()).delete(uuid);

  @override
  Future<void> deleteAll() async => await (await _getBox()).clear();

  @override
  Future<ProductModel?> get(String uuid) async => (await _getBox()).get(uuid);

  @override
  Future<List<ProductModel>> getAll() async => (await _getBox()).values.toList();

  @override
  Future<ProductModel> save(ProductModel product) async {
    await (await _getBox()).put(product.uuid, product);
    return product;
  }

  @override
  Future<ProductModel> update(String uuid, ProductModel product) async {
    await (await _getBox()).put(uuid, product);
    return product;
  }
}
