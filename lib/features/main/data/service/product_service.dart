/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:compared/features/main/domain/model/product.dart';

abstract class ProductService {
  Future<ProductModel> save(ProductModel product);

  Future<ProductModel?> get(String uuid);

  Future<List<ProductModel>> getAll();

  Future<void> delete(String uuid);

  Future<void> deleteAll();

  Future<ProductModel> update(String uuid, ProductModel product);
}
