/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:compared/core/extensions/extensions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class ProductInfoFields extends HookWidget {
  const ProductInfoFields({
    super.key,
    required this.titleController,
    required this.priceController,
    required this.amountController,
    required this.formKey,
    required this.onEditingComplete,
  });

  final GlobalKey<FormState> formKey;
  final void Function() onEditingComplete;
  final TextEditingController titleController;
  final TextEditingController priceController;
  final TextEditingController amountController;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: Column(
        children: [
          TextFormField(
            controller: titleController,
            autofocus: true,
            focusNode: useFocusNode()
              ..addListener(() => titleController.selection = TextSelection(baseOffset: 0, extentOffset: titleController.text.length)),
            textInputAction: TextInputAction.next,
            decoration: const InputDecoration(
              label: Text('Название'),
              hintText: 'Товар 1',
            ),
            validator: (value) {
              value = value?.trim();
              return value == null || value.isEmpty ? 'Пожалуйста, заполните поле' : null;
            },
          ),
          TextFormField(
            controller: priceController,
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.next,
            decoration: const InputDecoration(
              label: Text('Цена'),
              hintText: '100.00',
            ),
            validator: (value) {
              value = value?.trim();
              if (value == null || value.isEmpty) {
                return 'Пожалуйста, заполните поле';
              }
              if (value.tryParseToDouble() == null) {
                return 'Пожалуйста, проверьте корректность ввода';
              }
              return null;
            },
            focusNode: useFocusNode()
              ..addListener(() => priceController.selection = TextSelection(baseOffset: 0, extentOffset: priceController.text.length)),
          ),
          TextFormField(
              controller: amountController,
              keyboardType: TextInputType.number,
              onEditingComplete: onEditingComplete,
              decoration: const InputDecoration(
                label: Text('Количество'),
                hintText: '1.0',
              ),
              validator: (value) {
                value = value?.trim();
                if (value == null || value.isEmpty) {
                  return 'Пожалуйста, заполните поле';
                }
                if (value.tryParseToDouble() == null) {
                  return 'Пожалуйста, проверьте корректность ввода';
                }
                return null;
              },
              focusNode: useFocusNode()
                ..addListener(() => amountController.selection = TextSelection(baseOffset: 0, extentOffset: amountController.text.length))),
        ],
      ),
    );
  }
}
