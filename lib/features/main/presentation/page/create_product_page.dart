/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:compared/core/constants/sizes.dart';
import 'package:compared/core/extensions/extensions.dart';
import 'package:compared/features/main/domain/model/create_product.dart';
import 'package:compared/features/main/domain/provider/product_provider.dart';
import 'package:compared/features/main/presentation/components/product_info_fields.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class CreateProductPage extends HookConsumerWidget {
  const CreateProductPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final titleController = useTextEditingController(text: 'Товар ${(ref.read(productPod).valueOrNull?.products.length ?? 0) + 1}');
    final priceController = useTextEditingController();
    final amountController = useTextEditingController(text: '1');
    final formKey = GlobalKey<FormState>();
    onEditingComplete() {
      if (formKey.currentState?.validate() ?? false) {
        ref.read(productPod.notifier).saveProduct(
          CreateProductModel(
            title: titleController.text,
            amount: amountController.text.parseToDouble(),
            price: priceController.text.parseToDouble(),
          ),
        );
        Navigator.pop(context);
      }
    }
    return Scaffold(
      appBar: AppBar(title: const Text('Создание товара')),
      body: Padding(
        padding: const EdgeInsets.all(XLPadding),
        child: Column(children: [
          ProductInfoFields(
            formKey: formKey,
            titleController: titleController,
            priceController: priceController,
            amountController: amountController, onEditingComplete: onEditingComplete,
          ),
          FilledButton(
            onPressed: onEditingComplete,
            child: const Text('Сохранить'),
          )
        ]),
      ),
    );
  }
}
