/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:compared/core/constants/sizes.dart';
import 'package:compared/core/extensions/extensions.dart';
import 'package:compared/features/main/domain/provider/product_provider.dart';
import 'package:compared/features/main/presentation/components/product_info_fields.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class EditProductPage extends HookConsumerWidget {
  const EditProductPage({super.key, required this.productUUID});

  final String productUUID;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final product = ref.read(productPod).requireValue.productByUUID(productUUID);
    final titleController = useTextEditingController(text: product.title);
    final priceController = useTextEditingController(text: product.price.toStringAsFixed(2));
    final amountController = useTextEditingController(text: product.amount.toStringAsFixed(1));
    final formKey = GlobalKey<FormState>();
    onEditingComplete() {
      if (formKey.currentState?.validate() ?? false) {
        ref.read(productPod.notifier).updateProduct(
          product.copyWith(
            title: titleController.text,
            amount: amountController.text.parseToDouble(),
            price: priceController.text.parseToDouble(),
          ),
        );
        Navigator.pop(context);
      }
    }

    return Scaffold(
      appBar: AppBar(title: const Text('Редактирование товара')),
      body: Padding(
        padding: const EdgeInsets.all(XLPadding),
        child: Column(children: [
          ProductInfoFields(
            formKey: formKey,
            titleController: titleController,
            priceController: priceController,
            amountController: amountController,
            onEditingComplete: onEditingComplete,
          ),
          FilledButton(
            onPressed: onEditingComplete,
            child: const Text('Сохранить'),
          )
        ]),
      ),
    );
  }
}
