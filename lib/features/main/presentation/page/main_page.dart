/*
 * Copyright © 2024 Pavel Vetlugaev. Contacts: <pavetlugaev@gmail.com>. All rights reserved.
 */

import 'package:animated_list_plus/animated_list_plus.dart';
import 'package:animated_list_plus/transitions.dart';
import 'package:compared/features/main/domain/model/product.dart';
import 'package:compared/features/main/domain/provider/product_provider.dart';
import 'package:compared/features/main/presentation/page/create_product_page.dart';
import 'package:compared/features/main/presentation/page/edit_product_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:compared/core/core.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class MainPage extends ConsumerWidget {
  const MainPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Сравнение цен'),
          actions: [
            Consumer(
              builder: (context, ref, child) {
                if (ref.watch(productPod.select((value) => value.valueOrNull?.products.isNotEmpty ?? false))) {
                  return Tooltip(
                    message: 'Очистить список товаров',
                    child: IconButton(
                        onPressed: () async {
                          final result = await showDialog<bool>(
                            context: context,
                            builder: (context) => AlertDialog(
                              title: const Text('Очистить список?'),
                              content: const Text('После подтверждения, все товары из списка сравнения будут удалены'),
                              actions: [
                                TextButton(
                                  onPressed: () => Navigator.pop(context, true),
                                  child: Text(
                                    'Удалить',
                                    style: Theme.of(context).textTheme.bodyMedium?.copyWith(color: Theme.of(context).colorScheme.error),
                                  ),
                                ),
                                TextButton(
                                  onPressed: () => Navigator.pop(context, false),
                                  child: const Text('Отмена'),
                                )
                              ],
                            ),
                          );
                          if (result ?? false) {
                            ref.read(productPod.notifier).clearProducts();
                          }
                        },
                        icon: const Icon(Icons.delete)),
                  );
                }
                return const SizedBox();
              },
            ),
          ],
        ),
        body: Consumer(
          builder: (context, ref, child) {
            return ref.watch(productPod).when(
                data: (state) {
                  final products = state.products;
                  if (products.isEmpty) {
                    return Center(
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: XXLPadding),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              'assets/app_icon.svg',
                              width: 250,
                            ),
                            Text(
                              'Добавьте товары для сравнения',
                              style: Theme.of(context).textTheme.titleLarge?.copyWith(fontWeight: FontWeight.bold),
                            ),
                            Text(
                              'Нажмите на кнопку в правом нижнем углу',
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(color: Theme.of(context).disabledColor),
                            ),
                          ],
                        ),
                      ),
                    );
                  }
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: MPadding),
                    child: ImplicitlyAnimatedList<ProductModel>(
                      items: products,
                      areItemsTheSame: (ProductModel oldItem, ProductModel newItem) => oldItem == newItem,
                      itemBuilder: (context, animation, item, index) => SizeFadeTransition(
                        sizeFraction: 0.7,
                        curve: Curves.easeInOut,
                        animation: animation,
                        child: ItemCard(uuid: item.uuid, product: item),
                      ),
                    ),
                  );
                },
                error: (error, stackTrace) => Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text('Произошла ошибка при загрузке списка товаров'),
                          TextButton(onPressed: () => ref.invalidate(productPod), child: const Text('Обновить'))
                        ],
                      ),
                    ),
                loading: () => const Center(child: CircularProgressIndicator()));
          },
        ),
        floatingActionButton: Consumer(
          builder: (context, ref, child) {
            if (ref.watch(productPod.select((value) => value.valueOrNull?.products.isEmpty ?? true))) {
              return FloatingActionButton(
                child: const Icon(Icons.add),
                onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => const CreateProductPage())),
              );
            }
            return SpeedDial(
              activeIcon: Icons.close,
              spacing: SPadding,
              children: [
                SpeedDialChild(
                  child: const FaIcon(FontAwesomeIcons.sort),
                  label: 'Сортировка',
                  onTap: () => ref.read(productPod.notifier).sort(),
                ),
                SpeedDialChild(
                  child: const Icon(Icons.add),
                  label: 'Новый товар',
                  onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => const CreateProductPage())),
                ),
              ],
              child: const Icon(Icons.menu),
            );
          },
        ),
      ),
    );
  }
}

class ItemCard extends HookConsumerWidget {
  const ItemCard({
    super.key,
    required this.uuid,
    required this.product,
  });

  final String uuid;
  final ProductModel product;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final compare = ref.read(productPod).requireValue.benefitRelativeToOtherProducts(product);
    return GestureDetector(
      onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => EditProductPage(productUUID: uuid))),
      child: Card(
        child: Stack(
          fit: StackFit.passthrough,
          children: [
            Padding(
              padding: const EdgeInsets.all(LPadding),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: XXLPadding),
                    child: Text(product.title, style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.bold)),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          const Text('Цена за единицу'),
                          Tooltip(
                            triggerMode: TooltipTriggerMode.tap,
                            message: 'Ниже = выгодней',
                            child: Icon(Icons.question_mark, color: Theme.of(context).disabledColor, size: 15),
                          ),
                          Text(': ${priceFormat().format(product.priceForOne)}'),
                        ],
                      ),
                      Row(
                        children: [
                          const Text('Кол-во за одну денюжку'),
                          Tooltip(
                            triggerMode: TooltipTriggerMode.tap,
                            message: 'Больше = выгодней',
                            child: Icon(Icons.question_mark, color: Theme.of(context).disabledColor, size: 15),
                          ),
                          Text(': ${product.piecesPerPriceUnit.toStringAsFixed(2)}'),
                        ],
                      ),
                      Text('Цена: ${priceFormat().format(product.price)}'),
                      Text('Количество: ${product.amount.toStringAsFixed(1)}'),
                      const SizedBox(height: MPadding),
                      if (ref.read(productPod).requireValue.products.length > 1) Row(
                        children: [
                          const Text('Скидка относительно других товаров'),
                          Tooltip(
                            triggerMode: TooltipTriggerMode.tap,
                            showDuration: const Duration(seconds: 10),
                            message: '+ -> насколько текущий товар дешевле относительно указанного товара\n'
                                '- -> насколько указанный товар дешевле текущего',
                            child: Icon(Icons.question_mark, color: Theme.of(context).disabledColor, size: 15),
                          ),
                          const Text(':'),
                        ],
                      ),
                      ListView.builder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemCount: compare.length,
                        itemBuilder: (context, index) {
                          final discount = compare[index].discount < 0 ? compare[index].priceDifference / product.price : compare[index].discount;
                          return RichText(
                            text: TextSpan(
                              text: '${compare[index].product.title}: ',
                              style: Theme.of(context).textTheme.bodyMedium,
                              children: [
                                TextSpan(
                                  text: '${priceFormat().format(compare[index].priceDifference)} (${(discount * 100).toStringAsFixed(1)}%)',
                                  style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                                        color: switch (discount) {
                                          > 0 => Theme.of(context).custom.success,
                                          < 0 => Theme.of(context).colorScheme.error,
                                          _ => Theme.of(context).disabledColor
                                        },
                                      ),
                                ),
                              ],
                            ),
                          );
                        },
                      )
                    ],
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.topRight,
              child: IconButton(
                onPressed: () => ref.read(productPod.notifier).deleteProduct(product.uuid),
                icon: const Icon(Icons.delete_forever_rounded),
              ),
            )
          ],
        ),
      ),
    );
  }
}
